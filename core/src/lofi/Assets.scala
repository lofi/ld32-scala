package lofi

import com.badlogic.gdx.graphics.Texture


object Assets {
  object textures {
    val balloon = new Texture("balloon.png")
    val basic_bullet = new Texture("basic_bullet.png")
    val basic_bullet_2 = new Texture("basic_bullet_2.png")
    val eyeball = new Texture("eyeball.png")
    val fireball = new Texture("fire_ball.png")
    val glasses = new Texture("silly_glasses.png")
    val house = new Texture("house.png")
    val monster = new Texture("avery_monster.png")
    val pie = new Texture("pie.png")
    val shroom = new Texture("shroom.png")
    val zuper = new Texture("super.png")
    val player = new Texture("player.png")
  }

  val bullet_textures = Seq(
    textures.balloon,
    textures.basic_bullet,
    textures.basic_bullet_2,
    textures.fireball
  )
}
