package lofi

import com.badlogic.gdx.{Game, Gdx}
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.{BitmapFont, SpriteBatch}


class RandoShmupGame extends Game {

	lazy val batch = new SpriteBatch()
	lazy val font = new BitmapFont()  // FIXME: get the mincraftia font loaded instead of the default
	lazy val player =  new Player()

	override def create (): Unit = {
    setScreen(new lofi.state.Menu(this))
  }

	override def dispose(): Unit = {
    batch.dispose()
    font.dispose()
  }

	override def pause() {}

	override def resume() {}

	override def resize(width: Int, height: Int) {}
}
