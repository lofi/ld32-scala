package lofi.state

import lofi.RandoShmupGame
import com.badlogic.gdx.Gdx

class Menu(val game: RandoShmupGame) extends GameScreen {

  implicit val batch = game.batch

  override def onRender(implicit delta: Float): Unit = {

    game.font.draw(batch, "START", framebuffer.getWidth / 2, framebuffer.getHeight / 2)

    if (Gdx.input.isTouched) {
      game.setScreen(new Play(game))
    }
  }

}
