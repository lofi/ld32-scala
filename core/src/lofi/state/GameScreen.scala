package lofi.state

import com.badlogic.gdx.graphics.Pixmap.Format
import com.badlogic.gdx.graphics.Texture.TextureFilter
import com.badlogic.gdx.graphics.{GL20, OrthographicCamera}
import com.badlogic.gdx.graphics.g2d.{TextureRegion, SpriteBatch}
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import com.badlogic.gdx.math.{Vector2, Matrix4}
import com.badlogic.gdx.utils.Scaling
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.{Gdx, Screen}

trait GameScreen extends Screen {

  implicit val batch: SpriteBatch
  implicit var camera: OrthographicCamera = null

  var framebuffer: FrameBuffer = null
  var screen: TextureRegion = null
  val screenspace: Matrix4 = new Matrix4()
  val region: Vector2 = new Vector2()

  def onUpdate(implicit delta: Float): Unit = {}
  def onRender(implicit delta: Float): Unit = {}
  def onShow(): Unit = {}
  def onHide(): Unit = {}
  def onPause(): Unit = {}
  def onResume(): Unit = {}
  def onDispose(): Unit = {}

  def render(delta: Float): Unit = {
    // Update the game state
    onUpdate(delta)

    ////////////////////////////////////////////
    // Render to the offscreen buffer
    framebuffer.begin()

    Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

    batch.setProjectionMatrix(camera.combined)
    batch.begin()

    onRender(delta)

    batch.end()

    framebuffer.end()
    ///////////////////////////////////////////

    ///////////////////////////////////////////
    // Copy to the screen
    batch.setProjectionMatrix(screenspace)
    batch.begin()

    Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

    batch.draw(screen, 0, 0, region.x, region.y)

    batch.end()
    ///////////////////////////////////////////
  }

  def resize(width: Int, height: Int): Unit = {}

  def show(): Unit = {
    val framebuffer_width = 1280 / 2
    val framebuffer_height = 720 / 2
    val screen_width = Gdx.graphics.getWidth
    val screen_height = Gdx.graphics.getHeight

    camera = new OrthographicCamera()
    camera.setToOrtho(false, framebuffer_width, framebuffer_height)

    framebuffer = new FrameBuffer(Format.RGBA8888, framebuffer_width, framebuffer_height, false)
    framebuffer.getColorBufferTexture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest)

    screen = new TextureRegion(
      framebuffer.getColorBufferTexture,
      0, 0, framebuffer_width, framebuffer_height
    )
    screen.flip(false, true)

    screenspace.setToOrtho2D(0, 0, screen_width, screen_height)
    region.set( Scaling.fit(
      framebuffer_width, framebuffer_height,
      screen_width, screen_height
    ))

    onShow()
  }

  def hide(): Unit = {
    onHide()
  }

  def pause(): Unit = {
    onPause()
  }

  def resume(): Unit = {
    onResume()
  }

  def dispose(): Unit = {
    onDispose()

    framebuffer.dispose()
    screen = null
    framebuffer = null
    camera = null
  }
}
