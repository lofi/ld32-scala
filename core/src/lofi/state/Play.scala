package lofi.state

import com.badlogic.gdx.math.{Matrix4, Vector2}
import com.badlogic.gdx.utils.Scaling
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.{Gdx, Screen}
import com.badlogic.gdx.graphics.{GL20, OrthographicCamera}
import lofi.RandoShmupGame

class Play(val game: RandoShmupGame) extends GameScreen {

  implicit val batch = game.batch

  override def onUpdate(implicit delta: Float) = game.player.update
  override def onRender(implicit delta: Float) = game.player.draw

  override def onShow(): Unit = {
    game.player.setPosition(0,0)
  }

}
