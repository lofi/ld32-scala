package lofi

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.{Camera, Texture}
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.{MathUtils, Vector3, Vector2, Rectangle}
import com.badlogic.gdx.utils.{Pool, Pools, Array}
import lofi.weapon.Weapon
import lofi.weapon.Bullet


class Player extends Rectangle {

  final val max_bullets = 500

  width = 32f
  height = 32f
  setCenter(width / 2, height / 2)

  val movement_speed = 500.0f

  private final val active_bullets: Array[Bullet] = new Array[Bullet]
  private final val bullet_pool: Pool[Bullet] = Pools.get(classOf[Bullet], max_bullets)

  def bomb(): Unit = ???
  def setWeapon(weapon: Weapon) = ???

  def setupGamePad(): Unit = ???
  def setupKeyboard(): Unit = ???

  def update(implicit camera: Camera, delta: Float): Unit = {
    if(Gdx.input.isKeyPressed(Keys.LEFT)) x -= movement_speed * delta
    if(Gdx.input.isKeyPressed(Keys.RIGHT)) x += movement_speed * delta
    if(Gdx.input.isKeyPressed(Keys.UP)) y += movement_speed * delta
    if(Gdx.input.isKeyPressed(Keys.DOWN)) y -= movement_speed * delta
    if(Gdx.input.isKeyPressed(Keys.SPACE)) fire()
    if(Gdx.input.isTouched) {
      val touch = new Vector3(Gdx.input.getX, Gdx.input.getY, 0)

      camera.unproject(touch)

      x = touch.x
      y = touch.y

      fire()
    }

    // we count in reverse here because "dead" bullets get removed.
    var bullet: Bullet = null
    for (i <- active_bullets.size-1 to 0 by -1) {
      bullet = active_bullets.get(i)
      bullet.update()
      if (bullet.x > Gdx.graphics.getWidth) {
        //println(s"Freeing bullet $i")
        active_bullets.removeIndex(i)
        bullet_pool.free(bullet)
      }
    }
  }

  def draw(implicit batch: SpriteBatch): Unit = {
    batch.draw(Assets.textures.player, x, y, width, height)

    for (i <- 0 until active_bullets.size) {
      active_bullets.get(i).draw()
    }
  }

  def fire(): Unit = {
    val bullet = bullet_pool.obtain()
    bullet.fire(x, y, 0f)
    active_bullets.add(bullet)
  }
  
}
