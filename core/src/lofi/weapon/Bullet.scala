package lofi.weapon

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Pool.Poolable
import lofi.Assets

class Bullet extends Rectangle with Poolable {

  val movement_speed: Float = 1000.0f
  var acceleration: Float = 0f
  var velocity: Float = 0f
  var texture: Texture = null

  var srcX: Float = 0f
  var srcY: Float = 0f
  var angle: Float = 0f

  // initialize
  reset()

  override def reset(): Unit = {
    width = 32f
    height = 32f
    setCenter(width / 2, height / 2)

    acceleration = 0f
    velocity = 0f

    srcX = 0f
    srcY = 0f
    angle = 0f
    texture = Assets.textures.basic_bullet
  }

  def fire(srcX: Float, srcY: Float, this_angle: Float): Unit = {
    x = srcX
    y = srcY
    angle = this_angle
    velocity = movement_speed
  }

  def update()(implicit delta: Float): Unit = {
    velocity += acceleration * delta
    val velocityX = velocity * Math.cos(angle)
    val velocityY = velocity * Math.sin(angle)
    x += ((velocityX.toFloat*delta) + .5*acceleration*Math.pow(delta.toDouble, 2d)).toFloat
    y += ((velocityY.toFloat*delta) + .5*acceleration*Math.pow(delta.toDouble, 2d)).toFloat
  }

  def draw()(implicit batch: SpriteBatch): Unit = {
    batch.draw(texture, x, y, width, height)
  }

}
