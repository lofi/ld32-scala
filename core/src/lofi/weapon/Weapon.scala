package lofi.weapon

import com.badlogic.gdx.graphics.Texture
import scala.util.Random
import lofi.Assets


case class WeaponData(fireRate: Float,
                      spriteScale: Float,
                      bulletSpeed: Float,
                      scaleSpeed: Float,
                      bulletBankScale: Float)


class WeaponBehavior {}

class Weapon(behavior: WeaponBehavior, configuration: WeaponData, texture: Texture) {}

object Weapon {
  private val _rng = new Random(System.currentTimeMillis())
  private def _randomChoice[T](s: Seq[T]): T = s(_rng.nextInt(s.length))

  val configurations = Seq(
    WeaponData(150, 2, 600, 0, 3),  // big and slow
    WeaponData(150, 1, 900, 10, 1),  // growing bullets
    WeaponData(300, 0.5f, 900, 0, 3)  // high rate of fire
  )
  val behaviors = Seq(
    new StraightForward,
    new Spread,
    new BackAndForth,
    new Circle
  )

  def nextRandom(): Weapon = {
    val texture = _randomChoice(Assets.bullet_textures)
    val config = _randomChoice(configurations)
    val behavior = _randomChoice(behaviors)

    new Weapon(behavior, config, texture)
  }

}

