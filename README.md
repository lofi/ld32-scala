Rando Shmup (Scala)
===================

Basically, this is a straight port of our earlier ld32 entry, which was
implemented in ES 6 and Phaser.

Initial passes will simply update the initial APIs so they work in scala and 
libgdx, but additional passes may be made to make the codebase a little more
idiomatic.

Steps for setting up your dev environment
-----------------------------------------

This documentation will cover steps I took to get the game building and 
integration with IntelliJ IDEA.

1. Install a JDK (I'm using Java 8).
1. Install gradle (I unpacked the binary dist to `~/apps/` and symlinked the 
   gradle binary to `~/bin/` which is in my `PATH`).
1. Install the android sdk set `$ANDROID_HOME` to the directory where you 
   unpacked it.
1. Invoke `$ANDROID_HOME/tools/android` and select some android platform 
   versions to download (we're going to be targeting __4.1 and up__ I think).
1. Invoke `./gradlew idea` to generate IDEA project and module config files.
1. Open IDEA and ask it to open the `gradle.build` in the repo directory. 
   The import dialog should pop up - defaults should be fine, but I _did_ ask 
   it to favor the `.ipr` (file based) project file.
1. With the project open, add a new gradle based run configuration. Point it at
   the main `gradle.build` and enter `desktop:run` as the task to run.
   
If you run this run configuration in debug mode using the bug icon next to the 
play icon, you will be able to use _jvm hotswap_ to reload class definitions 
while the game is running. In order for this to work, start the game in debug 
mode (hotkey shift+F9) then make edits to the code and trigger a recompile 
(ctrl+shift+F9). If you search for "hotswap" in the settings dialog, there are 
options that will let you skip a _"reload classes?"_ prompt (basically I 
checked all the boxes in this section).

I haven't done anything with the android build yet, so more to come there as I 
learn what's required to get it going. At a minimum, you'll have to open the 
project structure dialog and add your `$ANDROID_HOME` directory as an SDK and 
select that SDK for the android module in your project. Additional steps will
be required to configure virtual devices, or to execute code on an actual 
device.